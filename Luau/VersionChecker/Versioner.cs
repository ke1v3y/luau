﻿namespace VersionWriter
{
    class Program
    {
        static void Main(string[] args)
        {
            string version = System.String.Empty;

            try
            {
                version = System.Diagnostics.FileVersionInfo.GetVersionInfo(args[0]).FileVersion.ToString();
            } catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.ToString());
            }

            System.Console.Out.Write(version);
            return;
        }
    }
}
