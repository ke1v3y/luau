﻿using System;

namespace LuauUpdateManager
{
    internal class UpdateManager
    {
        private static void Main(string[] args)
        {
            string appVersion = String.Empty;
            string batchPath = args[0];
            string applicationPath = args[1];
            string safeBatchPath = "\"" + args[0] + "\"";
            string safeApplicationPath = "\"" + args[1] + "\"";

            System.Threading.Thread.Sleep(1500);

            try
            {
                appVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(applicationPath).FileVersion.ToString();
                appVersion = null;
            }
            catch/* (Exception ex)*/
            {
                throw new Exception("ApplicationPathError");
            }
            
            System.Diagnostics.ProcessStartInfo processConfig = new System.Diagnostics.ProcessStartInfo("cmd.exe");
            processConfig.RedirectStandardOutput = false;
            processConfig.UseShellExecute = false;
            processConfig.CreateNoWindow = false;
            processConfig.RedirectStandardError = true;
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            processConfig.Arguments = "/c " + safeBatchPath;
            process.StartInfo = processConfig;
            process.Start();
            process.WaitForExit();
            
            switch (process.ExitCode)
            {
                case 0:
                    Console.WriteLine("Update successful. Starting application...");
                    break;
                default:
                    Console.WriteLine("Update failed. This update will be reattempted" +
                        " at a later date. Restarting application...");
                    System.IO.File.WriteAllText(@"C:\Unipresence\log.txt",
                        process.StandardError.ReadToEnd());
                    System.Diagnostics.Process.Start(applicationPath);
                    break;
            }

            return;
        }
    }
}
