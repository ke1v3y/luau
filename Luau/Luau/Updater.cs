﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luau
{
    /// <summary>
    /// Use this class in your application's controller code or highest level
    /// implementation. Setup the Updater object in your Main module, and add
    /// the Luau.exe output to your post-build events.
    /// </summary>
    /// <example>
    /// <code>
    /// // Add this to your Post-Build events:
    /// "{PATH TO PACKAGE FOLDER}\LuauVersionWriter.exe" $(TargetPath) > version.txt
    /// </code>
    /// <code>
    /// // Add this block of code to your main controller:
    ///  string version = System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.
    ///      Assembly.GetExecutingAssembly().Location).ProductVersion;
    ///  string versionPath = @"C:\Users\ke1v3\Documents\Compiled Apps\Practicar\version.txt";
    ///  string batchPath = @"C:\Users\ke1v3\Documents\sample.bat";
    ///  string applicationPath = @"C:\Users\ke1v3\Documents\Compiled Apps\Practicar\Practicar.exe";
    ///
    ///  if (Luau.Updater.VersionChecker(versionPath, version))
    ///  {
    ///      System.Diagnostics.Process.Start(@"C:\Users\ke1v3\Documents\Compiled Apps\Practicar\LuauUpdateManager.exe",
    ///          "\"" + @batchPath + "\" \"" + @applicationPath + "\"");
    ///      return;
    ///  }
    /// </code>
    /// </example>
    public class Updater
    {
        /// <summary>
        /// Checks to see if a specified application has a more current version
        /// </summary>
        /// <param name="pathToFileVersion">Path to build version file.<see cref="Updater"/></param>
        /// <param name="currentVersion">Current application version</param>
        /// <param name="isWebPath">Represents whether the version file located on the web</param>
        /// <returns>Whether or not the application version has a new version available</returns>
        public static bool VersionChecker(string pathToFileVersion, string currentVersion, bool isWebPath = false)
        {
            bool newVersionAvailable;
            string latestVersion;

            if (isWebPath)
            {
                using (System.Net.WebClient client = new System.Net.WebClient())
                using (Stream stream = client.OpenRead(pathToFileVersion))
                using (StreamReader reader = new StreamReader(stream))
                {
                    latestVersion = reader.ReadToEnd();
                }
            } else
            {
                latestVersion = File.ReadAllText(pathToFileVersion);
            }


            if (currentVersion == latestVersion)
            {
                newVersionAvailable = false;
            }
            else
            {
                newVersionAvailable = true;
            }

            return newVersionAvailable;
        }
    }
}
